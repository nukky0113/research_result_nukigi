let img;
let poseNet;
let poses = [];

var csv = 'data/walk_model30.jpg';
//var csv = 'data/sample_walk.jpg';

console.log(csv);
//var csv = 'data/hokou.jpg';
var keypoint_csv = 'keypoint_walk30.csv';
var skeleton_csv = 'skeleton_walk30.csv';
function setup() {
    //createCanvas(793, 413);
    createCanvas(600,300);
    // create an image using the p5 dom library
    // call modelReady() when it is loaded
    img = createImg(csv, imageReady);
    // set the image size to the size of the canvas
    img.size(width, height);

    img.hide(); // hide the image in the browser
    frameRate(1); // set the frameRate to 1 since we don't need it to be running quickly in this case
}

// when the image is ready, then load up poseNet
function imageReady(){
    // set some options
    let options = {
        imageScaleFactor: 1,
        minConfidence: 0.1
    }
    
    // assign poseNet
    poseNet = ml5.poseNet(modelReady, options);
    // This sets up an event that listens to 'pose' events
    poseNet.on('pose', function (results) {
        poses = results;
        //console.log(poses);
    });
}

// when poseNet is ready, do the detection
function modelReady() {
    select('#status').html('Model Loaded');
     
    // When the model is ready, run the singlePose() function...
    // If/When a pose is detected, poseNet.on('pose', ...) will be listening for the detection results 
    // in the draw() loop, if there are any poses, then carry out the draw commands
    poseNet.singlePose(img)
}

// draw() will not show anything until poses are found
function draw() {
    if (poses.length > 0) {
        image(img, 0, 0, width, height);
        drawSkeleton(poses);
        drawKeypoints(poses);
        noLoop(); // stop looping when the poses are estimated
    }

}

// The following comes from https://ml5js.org/docs/posenet-webcam
// A function to draw ellipses over the detected keypoints
function drawKeypoints() {
    var array = [];
    var array2 = [];
    var header = [];
    // Loop through all the poses detected
    for (let i = 0; i < poses.length; i++) {
        // For each pose detected, loop through all the keypoints
        let pose = poses[i].pose;
        for (let j = 0; j < pose.keypoints.length; j++) {
            // A keypoint is an object describing a body part (like rightArm or leftShoulder)
            let keypoint = pose.keypoints[j];
            if(j == 0){
                header.push(['part','score','x','y']);
                array.push([keypoint.part,keypoint.score,keypoint.position.x,keypoint.position.y]);
                //array.push(keypoint.part);
                //array.push(keypoint.position.x);
                //array.push(keypoint.position.y);
                pushTwoDimensionalArray(header,array);
            }
            else{
                array2.push([keypoint.part,keypoint.score,keypoint.position.x,keypoint.position.y]);
                //array2.push(keypoint.score);
                //array2.push(keypoint.part);
                //array2.push(keypoint.position.x);
                //array2.push(keypoint.position.y);
                pushTwoDimensionalArray(array, array2);
            }
            array2 = [];
            // Only draw an ellipse is the pose probability is bigger than 0.2
            if (keypoint.score > 0.2) {
                fill(255);
                stroke(20);
                strokeWeight(4);
                ellipse(round(keypoint.position.x), round(keypoint.position.y), 8, 8);
            }
        }
        //console.log(array)
        (new CSV(array)).save(keypoint_csv);
    }
}

// A function to draw the skeletons
function drawSkeleton() {
    var array = [];
    var array2 = [];
    var header = [];
    // Loop through all the skeletons detected
    for (let i = 0; i < poses.length; i++) {
        let skeleton = poses[i].skeleton;
        // For every skeleton, loop through all body connections
        for (let j = 0; j < skeleton.length; j++) {
            let partA = skeleton[j][0];

            let partB = skeleton[j][1];

            if(j == 0){
                header.push(['partA.x','partA.y','partB.x','partB.y']);
                array.push([partA.position.x, partA.position.y, partB.position.x, partB.position.y]);
                //array.push(keypoint.part);
                //array.push(keypoint.position.x);
                //array.push(keypoint.position.y);
                pushTwoDimensionalArray(header,array);
            }
            else{
                array2.push([partA.position.x, partA.position.y, partB.position.x, partB.position.y]);
                //array2.push(keypoint.score);
                //array2.push(keypoint.part);
                //array2.push(keypoint.position.x);
                //array2.push(keypoint.position.y);
                pushTwoDimensionalArray(array, array2);
            }
            array2 = [];

            stroke(255);
            strokeWeight(1);
            line(partA.position.x, partA.position.y, partB.position.x, partB.position.y);
        }
        (new CSV(array)).save(skeleton_csv);
    }
}

function pushTwoDimensionalArray(array1, array2, axis){
    if(axis != 1) axis = 0;
    if(axis == 0) {
        for(var i = 0; i < array2.length; i++){
            array1.push(array2[i]);
        }
    }
    else {
        for(var i = 0; i < array1.length; i++){
            Array.prototype.push.apply(array1[i],array2[i]);
        }
    }
}

class CSV {
  constructor(data, keys = false) {
    this.ARRAY  = Symbol('ARRAY')
    this.OBJECT = Symbol('OBJECT')

    this.data = data

    if (CSV.isArray(data)) {
      if (0 == data.length) {
        this.dataType = this.ARRAY
      } else if (CSV.isObject(data[0])) {
        this.dataType = this.OBJECT
      } else if (CSV.isArray(data[0])) {
        this.dataType = this.ARRAY
      } else {
        throw Error('Error: 未対応のデータ型です')
      }
    } else {
      throw Error('Error: 未対応のデータ型です')
    }

    this.keys = keys
  }

  toString() {
    if (this.dataType === this.ARRAY) {
      return this.data.map((record) => (
        record.map((field) => (
          CSV.prepare(field)
        )).join(',')
      )).join('\n')
    } else if (this.dataType === this.OBJECT) {
      const keys = this.keys || Array.from(this.extractKeys(this.data))

      const arrayData = this.data.map((record) => (
        keys.map((key) => record[key])
      ))

      console.log([].concat([keys], arrayData))

      return [].concat([keys], arrayData).map((record) => (
        record.map((field) => (
          CSV.prepare(field)
        )).join(',')
      )).join('\n')
    }
  }

  save(filename = 'data.csv') {
    if (!filename.match(/\.csv$/i)) { filename = filename + '.csv' }

    console.info('filename:', filename)
    console.table(this.data)

    const csvStr = this.toString()

    const bom     = new Uint8Array([0xEF, 0xBB, 0xBF]);
    const blob    = new Blob([bom, csvStr], {'type': 'text/csv'});
    const url     = window.URL || window.webkitURL;
    const blobURL = url.createObjectURL(blob);

    let a      = document.createElement('a');
    a.download = decodeURI(filename);
    a.href     = blobURL;
    a.type     = 'text/csv';

    a.click();
  }

  extractKeys(data) {
    return new Set([].concat(...this.data.map((record) => Object.keys(record))))
  }

  static prepare(field) {
    return '"' + (''+field).replace(/"/g, '""') + '"'
  }

  static isObject(obj) {
    return '[object Object]' === Object.prototype.toString.call(obj)
  }

  static isArray(obj) {
    return '[object Array]' === Object.prototype.toString.call(obj)
  }
}
