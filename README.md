# ウェアラブルデバイスを用いた姿勢可視化システム

スマートフォンを用いてスクワットの姿勢をGUIに表示するシステムである。

## 使用方法

### 使用環境・使用機器

* windows10
* Anaconda3
* android studio 3.5.3
* Google Nexus 5 



### インストール

プログラム必要なもの

* Step 1: pythonのライブラリをインストール

```
conda install pillow
```
```
conda install pandas
```
```
conda install scikit-learn
```

## 動作例

プログラムの動かし方

### ml5-examples

初めに、プロジェクトを保存したディレクトリに移動する
その後、次のコマンドをコマンドプロンプトに入力しディレクトリを移動する
```
cd　research_result_nukigi/ml5-examples
```
ディレクトリの移動が完了したら、次のコマンドを入力し、ローカルサーバを開く
```
python - m http.server
```
Webブラウザでhttp://localhost:8000をURLで指定して開くといろいろな選択肢が出てくる
画像から抽出した間接の座標をcsvファイルとしてダウンロードしたい場合は以下の手順で選択する
* p5js -> PoseNet -> PoseNet_image_single
画像や保存するcsvファイル名を変更したい場合は
p5js/PoseNet/PoseNet_image_singleの中を少し変更する
dataフォルダに関節の座標を抽出したい画像を保存する
sketch.jsのほうでは、関節の座標を抽出したい画像の名前を指定しcsvファイル名も自分が保存したい名前に変更する

画像から骨格推定を行いたい場合は以下の手順で選択する
* javascript -> PoseNet -> PoseNet_image_single
画像を変更したい場合は
javascript/PoseNet/PoseNet_image_singleの中を少し変更する
dataフォルダに関節の座標を抽出したい画像を保存する
indexのほうで、推定したい画像の名前を指定

### demo

混ざっているMainActivity.javaのソースコードはandroid studio用のソースコード

トリミングをしたい場合
```
python crop.py
```
androidからデータを受信しcsvファイルとして保存したい場合
```
python udp_recv.py
```
svmで学習モデルを作成したい場合
```
python svm.py
```
取得してきた姿勢モデルを確認したい場合
```
python test.py
```
姿勢の推定を行いたい場合先に学習モデルの作成が必要
```
python demo.py
```

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds


## Author

* **Sora Nukigi

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

