
import csv 
import pandas as pd
import numpy as np

header = []
body = []
average = []
row = []
cal = []

# 収集してきた姿勢データのcsvファイルを一つにまとめる
def sisei(filepath,count):
	sum1 = 0
	#header.extend(['前後の傾斜','model'])
	print(header)

	with open(filepath, newline ='') as f:
		reader = csv.reader(f)
		for r in reader:
			row.append(r)
	for d in row:
		body.append(d[2])
	body.sort()
	start = 0
	end = len(body)
	length = len(body)/10
	res = len(body) % 10
	judge = 0

	if res != 0:
		length += 1
		judge = 1

	for i in range(0,int(length)):

		if i == int(length-1) and judge == 1:
			for n in range(int(start),int(end)):
				sum1 += float(body[n])
			average.append([sum1/res,count])
			break

		for j in range(int(start),int(start+5)):
			cal.append(body[j])

		for k in range(int(end-5),int(end)):
			cal.append(body[k])

		for l in range(0,10):
			sum1 += float(cal[l])

		average.append([sum1/10,count])
		sum1 = 0
		start = j + 1
		end = k - 4
		cal.clear()
	
	row.clear()
	body.clear()
	print(average)

# 収集してきた種目データのcsvファイルを種目ごとにまとめる
def syumoku(filepath):
	#header = ['x軸','y軸','z軸','symoku']
	with open(filepath, newline ='') as f:
		reader = csv.reader(f)
		for r in reader:
			row.append(r)
		for d in row:
			body.append([d[2],d[3],d[4],d[5],d[6],d[7]])
	with open('data/demo/syumoku.csv','a',newline='', encoding="utf-8") as g:
		writer = csv.writer(g)
		#writer.writerow(header)
		writer.writerows(body)
	row.clear()
	body.clear()

# rangeの中には姿勢の数を指定
#for i in range(2):
#	filepath = 'data/model{}.csv'.format(i)
#	sisei(filepath,i)

#with open('data/sisei.csv','w',newline='') as f:
#		writer = csv.writer(f)
#		writer.writerow(header)
#		writer.writerows(average)

symoku = ['squ','walk']

for i in range(2):
	filepath = 'data/demo/' + symoku[i] + '.csv'
	syumoku(filepath) 
