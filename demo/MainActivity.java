package com.example.demo2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import android.media.AudioManager;
import android.media.ToneGenerator;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import java.util.Timer;
import java.util.TimerTask;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import java.util.ArrayList;

public class MainActivity extends Activity {

    private ToneGenerator toneGenerator = new ToneGenerator(
            AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
    private StringBuffer cbuf = new StringBuffer(128);
    int n;
    private DatagramSocket ds;
    private DatagramPacket dp;
    private String IP_remote;
    private int port_remote;

    // センサーマネージャー
    private SensorManager manager;

    // センサーイベントリスナー
    private SensorEventListener listener;

    // 加速度の値
    private float[] fAccell = null;

    // 地磁気の値
    private float[] fMagnetic = null;
    ArrayList<String> arrayList ;
    private static final int MATRIX_SIZE = 16;
    float[] inR = new float[MATRIX_SIZE];
    float[] outR = new float[MATRIX_SIZE];
    float[] I = new float[MATRIX_SIZE];
    int count = 1;
    //
    TextView textView1 = null;
    String sensor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IP_remote = "10.0.1.14"; //IPアドレス
        port_remote = 3000;       //ポート番号
        // センサーマネージャーを取得
        manager = (SensorManager)getSystemService(SENSOR_SERVICE);
        // センサーのイベントリスナーを登録
        listener = new SensorEventListener() {
            public void post() {
                SensorEvent sensorEvent;

            }
            // 値変更時
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                //if(count >= 10) {
                try {

                    //ds = new DatagramSocket();
                    //count = 0;

                    //InetAddress host = InetAddress.getByName(IP_remote);
                    //String message = "send by Android";
                    //ds = new DatagramSocket();
                    //byte[] data = message.getBytes();
                    //dp = new DatagramPacket(data, data.length, host, port_remote);  //DatagramPacket 作成

                    // センサーの種類で値を取得
                    switch (sensorEvent.sensor.getType()) {
                        // 加速度
                        case Sensor.TYPE_ACCELEROMETER:
                            fAccell = sensorEvent.values.clone();
                            break;

                        // 地磁気
                        case Sensor.TYPE_MAGNETIC_FIELD:
                            fMagnetic = sensorEvent.values.clone();
                            break;
                    }

                    // 両方確保した時点でチェック
                    if (fAccell != null && fMagnetic != null) {
                        // 回転行列を得る
                        SensorManager.getRotationMatrix(inR, I, fAccell, fMagnetic);

                        // ワールド座標とデバイス座標のマッピングを変換する
                        SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_X, SensorManager.AXIS_Y, outR);

                        // 姿勢を得る
                        float[] fAttitude = new float[3];
                        SensorManager.getOrientation(outR, fAttitude);

                        for (int i = 0; i < 3; i++) {
                            fAttitude[i] = (float) (fAttitude[i] * 180 / Math.PI);
                            fAttitude[i] = (fAttitude[i] < 0) ? fAttitude[i] + 360 : fAttitude[i];
                        }

                        // 出力内容を編集
                        String buf =
                                "---------- Orientation --------\n" +
                                        String.format("加速度_X\n\t%f\n", fAccell[0]) +
                                        String.format("加速度_Y\n\t%f\n", fAccell[1]) +
                                        String.format("加速度_Z\n\t%f\n", fAccell[2]) +
                                        String.format("方位角\n\t%f\n", fAttitude[0]) +
                                        String.format("前後の傾斜\n\t%f\n", fAttitude[1]) +
                                        String.format("左右の傾斜\n\t%f\n", fAttitude[2]);
                        textView1.setText(buf);
                        sensor = String.format("%f,%f,%f,%f,%f,%f",fAttitude[0],fAttitude[1],fAttitude[2],fAccell[0],fAccell[1],fAccell[2]);
                        new Thread(new Runnable() {
                            public void run() {
                                try{
                                    InetAddress host = InetAddress.getByName(IP_remote);
                                    String message = "send by Android " + n + " \n";  // 送信メッセージ
                                    ds = new DatagramSocket();  //DatagramSocket 作成
                                    byte[] data = sensor.getBytes();
                                    dp = new DatagramPacket(data, data.length, host, port_remote);  //DatagramPacket 作成
                                    ds.send(dp);
                                }catch(Exception e){
                                    System.err.println("Exception : " + e);
                                }
                            }
                        }).start();

                    }
                } catch (Exception e) {
                    System.err.println("Exception : " + e);
                }
                //}else count++;
            }

            //
            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };

        // text
        textView1 = (TextView)findViewById(R.id.text1);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // リスナー設定：加速度
        manager.registerListener (
                listener,
                manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                //SensorManager.SENSOR_DELAY_FASTEST);
                50000);

        // リスナー設定：地磁気
        manager.registerListener (
                listener,
                manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                //SensorManager.SENSOR_DELAY_FASTEST);
                50000);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // リスナー解除
        manager.unregisterListener(listener);
    }


    /*public void post_data(){
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            int count = 0;
            @Override
            public void run() {
                // UIスレッド
                count++;
                if (count > 5) { // 5回実行したら終了
                    return;
                }

                handler.postDelayed(this, 1000);
            }
        };
        handler.post(r);
    }*/


}