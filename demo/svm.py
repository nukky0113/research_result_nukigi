################################################
#
# svmで学習モデルの作成
#
################################################
from sklearn import datasets
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.svm import SVC # SVMを使用するためのインポート
from sklearn.linear_model import LogisticRegression # ロジスティック回帰を使用するためのインポート
from sklearn.metrics import accuracy_score
import pickle
import pandas as pd

def csv_reader(file_path):
    model_csv = pd.read_csv(file_path,header=None)
    return model_csv

#model = csv_reader('data/demo/demo_data.csv')
model = csv_reader('data/demo/sisei_squ.csv')
#種目推定用
#data = model.iloc[:,[0,1,2,3,4]]

#姿勢推定用
data = model.iloc[:,[0]]
model_data = np.array(data)

#種目推定用
#target = model[5]

#姿勢推定用
target = model[1]

model_target = np.array(target)
pos = np.array([[78]])

# トレーニングデータとテストデータに分割。
# 今回は訓練データを70%、テストデータは30%としている。
# 乱数を制御するパラメータ random_state は None にすると毎回異なるデータを生成する
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=None )
X_train, X_test, y_train, y_test = train_test_split(model_data, model_target, test_size=0.3, random_state=None )

# データの標準化処理
#sc = StandardScaler()
#sc.fit(X_train)
#X_train_std = sc.transform(X_train)
#X_test_std = sc.transform(X_test)

# 線形SVMのインスタンスを生成
model = SVC(kernel='linear', random_state=None)

# ロジスティック関数のインスタンスを生成
#model = LogisticRegression(random_state=None)

# モデルの学習。fit関数で行う。
model.fit(X_train, y_train)

with open('sisei_squ.pickle', mode='wb') as fp:
	pickle.dump(model, fp)

#with open('model.pickle', mode='rb') as fp:
#	clf = pickle.load(fp)
# トレーニングデータに対する精度
pred_train = model.predict(X_train)
accuracy_train = accuracy_score(y_train, pred_train)
print('トレーニングデータに対する正解率： %.2f' % accuracy_train)
#print(ayame)
# テストデータに対する精度
#pred_test = model.predict(pos)
#print(pred_test)
pred_test = model.predict(X_test)
accuracy_test = accuracy_score(y_test, pred_test)
print('テストデータに対する正解率： %.2f' % accuracy_test)