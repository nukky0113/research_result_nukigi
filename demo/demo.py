###############################################
#
# 推定した結果をGUIで可視化する
#
###############################################

import sys
import tkinter 
import pandas as pd
import csv
from time import sleep
from PIL import Image, ImageTk
import threading
from datetime import datetime
import socket
import struct
import pickle
import numpy as np

def csv_reader(key_file,skel_file):
    keypoint_csv = pd.read_csv(key_file,header=None)
    keypoint = keypoint_csv.values.tolist()
    keypoint_model = pd.DataFrame(data = keypoint, columns = ["part","score","x","y"])
    skeleton_csv = pd.read_csv(skel_file,header=None)
    skeleton = skeleton_csv.values.tolist()
    skeleton_model = pd.DataFrame(data = skeleton, columns = ["partA.x","partA.y","partB.x","partB.y"])

    #DataFrameをlist型に変換
    keypoint_model = keypoint_model.values.tolist()
    skeleton_model = skeleton_model.values.tolist()

    return keypoint_model,skeleton_model


#
# GUI設定
#
root = tkinter.Tk()
root.title(u"TkinterのCanvasを使ってみる")
root.geometry("800x400")

#キャンバスエリア
canvas = tkinter.Canvas(root, width = 800, height = 400)#Canvasの作成
#canvas.pack()
#キャンバスバインド
canvas.place(x=0, y=0)#Canvasの配置
count = 0
model = ["0","1","2","3","4","5","6","7","8","9",
        "10","11","10","9","8","7","6","5","4","3","2","1"]
sensor = []
global clf
global clf2


with open('syumoku.pickle', mode='rb') as fp2:
	clf2 = pickle.load(fp2)

def update():
	global count
	server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	# 自身でipアドレスを設定
	server.bind(('10.0.1.14', 3000))
	data= server.recvfrom(1024)
	server.close()
	#print('at', datetime.now(),'data: ', data)
	sensor = data[0].decode().split(',')
	#種目を推定
	#sport = np.array([[float(sensor[3]),float(sensor[4]),float(sensor[5])]])
	sport = np.array([[float(sensor[1]),float(sensor[2]),float(sensor[3]),float(sensor[4]),float(sensor[5])]])
	pref_sport = clf2.predict(sport)
	if pref_sport == 0:
		with open('sisei_squ.pickle', mode='rb') as fp:
			clf = pickle.load(fp)
		#姿勢推定
		pos = float(sensor[1])
		pos = np.array([[pos]])
		pref_pos = clf.predict(pos)
		
		canvas.create_rectangle(0, 0, 800, 400, fill = 'white')#塗りつぶし

		key_file='templates/model/keypoints/keypoint_squ{}.csv'.format(model[int(pref_pos)])
		skel_file='templates/model/skeletons/skeleton_squ{}.csv'.format(model[int(pref_pos)])
		keypoint,skelton = csv_reader(key_file,skel_file)
		
		for key in keypoint:
			canvas.create_oval(key[2]+5, key[3]+5, key[2]-5, key[3]-5, tag="oval0", fill = 'Blue')

		for skel in skelton:
			canvas.create_line(skel[0], skel[1], skel[2], skel[3], fill='black')

	if pref_sport == 1:
		canvas.delete("all")
	count += 1 
	if count > 21:
		count = 0
		canvas.delete("all")
		print(pref_sport)


	root.after(50,update)
update()
#
# GUIの末端
#
root.mainloop()